<%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/15
  Time: 17:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>结果</title>
    <%String message =(String) request.getAttribute("message");
    String requestUrl =(String) request.getAttribute("requestUrl");
    %>
    <script type="text/javascript">
        function myfun() {
            window.location.href='<%=requestUrl%>';
        }
        function myfun1() {
            let span1 = document.getElementById("span1");
            let val = span1.innerText;
            if(!isNaN(val)){
                val =parseInt(val);
            }
            val=val-1;
            span1.innerText=val;
            if(val==0){
                myfun()
            }
        }
        /*每个一秒执行一次*/
        setInterval(myfun1,1000);
    </script>
</head>
<body>
<h3><%=message%>,<span id="span1">3</span>秒后返回查询页</h3>
<button onclick="myfun()">立即跳转</button>
</body>
</html>
