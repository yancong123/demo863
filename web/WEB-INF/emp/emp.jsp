<%@ page import="com.soft.demoservlet.entity.Emp" %>
<%@ page import="java.util.List" %>
<%@ page errorPage="../../error.jsp"%>
<%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/14
  Time: 17:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%--WEB-INFO下的页面不能直接通过浏览器访问--%>
    <title>员工信息</title>
    <% List<Emp> list=(List<Emp>)request.getAttribute("emps");
        String account =(String) session.getAttribute("account");
    %>
</head>
<body>
<%=account%>
<a href="/emp?method=add">添加</a>
<table border="1px">
    <thead >
    <tr>
        <th>工号</th>
        <th>姓名</th>
        <th>年龄</th>
        <th>性别</th>
        <th>操作</th>
    </tr>
    </thead>
    <tbody>
    <%for(int i=0;i<list.size();i++){%>
        <tr>
            <td><%=list.get(i).getEno()%></td>
            <td><%=list.get(i).getEname()%></td>
            <td><%=list.get(i).getEage()%></td>
            <td><%=list.get(i).getEsex()%></td>
            <td><a href="/emp?method=delete&eno=<%=list.get(i).getEno()%>">删除</a>
                <a href="/emp?method=update&eno=<%=list.get(i).getEno()%>">修改</a></td>
        </tr>
    <% }%>
    </tbody>
</table>

</body>
</html>
