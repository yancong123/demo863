<%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/25
  Time: 14:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>学生信息</title>
    <%String url="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
    %>
    <script type="text/javascript" src="<%=url%>/js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            var page=1;//首页
            var count=0;//总条数
            var pageSize=3;//每页显示几条
            var endPage=0;//尾页
            var currentPage=0;//当前页
            /*当页面加载完成后执行该函数*/
            pageFun(page);
            /*当点击首页时执行该函数*/
            $("#first").click(function () {
                   pageFun(page);
            })
            /*当单击尾页时执行该函数*/
            $("#end").click(function () {
                pageFun(endPage);
            })
            /*当点击上一页时执行该函数*/
            $("#last").click(function () {
                if(currentPage==1){
                    alert("已经是第一页了");
                }else{
                    pageFun(currentPage-1);
                }

            })
            /*当点击下一页时执行该函数*/
            $("#next").click(function () {
                if(currentPage==endPage){
                    alert("已经是最后一页了");
                }else{
                    pageFun(currentPage+1);
                }

            })

            function pageFun(page){

               $.ajax({
                   url:"/student",
                   data:{method:'page',page:page,pageSize:3},
                   dataType:'json',
                   success:function(obj){
                       /*当前页面*/
                       currentPage=page;
                       $("#currentPage").text(currentPage);
                       /*获取总的数据行数*/
                       count = obj.count;
                       if(count==null){
                           return;
                       }
                       /*获取尾页页码*/
                       endPage=Math.ceil(count/pageSize);
                       let students = obj.students;
                       if(students==null){
                           return;
                       }
                       let val="";
                       for(let i=0;i<students.length;i++){
                           let updateuser=students[i].updateuser?students[i].updateuser:'';
                           let updatetime=students[i].updatetime?students[i].updatetime:'';
                           val=val+"<tr>" +
                               "<td>"+students[i].sno+"</td>"+
                               "<td>"+students[i].sname+"</td>"+
                               "<td>"+students[i].sage+"</td>"+
                               "<td>"+students[i].ssex+"</td>"+
                               "<td>"+students[i].insertime+"</td>"+
                               "<td>"+students[i].insertuser+"</td>"+
                               "<td>"+updatetime+"</td>"+
                               "<td>"+updateuser+"</td>"+
                               "</tr>"
                       }
                       $("#tbody1").html(val);
                   }
               });
           }
        })
    </script>
</head>
<body>
<table>
    <thead>
    <tr>
        <th>学号</th>
        <th>姓名</th>
        <th>年龄</th>
        <th>性别</th>
        <th>添加时间</th>
        <th>添加人</th>
        <th>修改时间</th>
        <th>修改人</th>
    </tr>
    </thead>
    <tbody id="tbody1">

    </tbody>
</table>
<button id="first">首页</button><button id="last">上一页</button><span id="currentPage"></span><button id="next">下一页</button><button id="end">尾页</button>
</body>
</html>
