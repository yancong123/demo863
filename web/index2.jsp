<%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/20
  Time: 15:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--指定作用域查找--%>
${sessionScope.name}
<P>${emp['ename']}</P>
<P>${emp.eno}</P>
<%--el表达式中可以进行算术运算--%>
<p>${emp.eage>10}</p>
<p>${message}</p>
<p>${applicationScope.list}</p>


<c:out value="这是c:out标签输出的内容"></c:out>
<%--设置值 var 声明的变量 value 值 target 目标对象 property 对象属性--%>
<c:set var="name" value="张三"></c:set>

<c:set target="${emp}" property="ename" value="李四"></c:set>

<P>${emp['ename']}</P>
${name}


<c:remove var="name" scope="page"></c:remove>

<c:if var="res" test="${emp.esex eq '男'}" >
    <input type="radio" name="esex" value="男" checked="checked"/>男
    <input type="radio" name="esex" value="女" />女
</c:if>
<c:if test="${not res}" >
    <input type="radio" name="esex" value="男" />男
    <input type="radio" name="esex" value="女" checked="checked"/>女
</c:if>

<c:choose>
    <c:when test="${score ge 90 and score le 100}">
        <p>优秀</p>
    </c:when>
    <c:when test="${score ge 80 and score lt 90}">
        <p>良好</p>
    </c:when>
    <c:when test="${score ge 70 and score lt 80}">
        <p>一般</p>
    </c:when>
    <c:when test="${score ge 60 and score lt 70}">
        <p>及格</p>
    </c:when>
    <c:otherwise>
        <p>不及格</p>
    </c:otherwise>
</c:choose>
${name}

<c:forEach items="${list}" var="emp">
    <p>${emp.ename}</p>
    <p>${emp.eage}</p>
    <p>${emp.esex}</p>
</c:forEach>
</body>
</html>
