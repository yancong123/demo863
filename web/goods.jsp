<%@ page import="com.soft.demoservlet.entity.Goods" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/19
  Time: 16:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>商品页</title>
    <script type="text/javascript">
       /* function myfun(id) {
            let p = document.querySelector("#"+id+">p");
            let span = document.querySelector("#"+id+">span");
        }*/
    </script>
    <style type="text/css">
        #div div{
            display: inline-block;
            width: 100px;
            height: 50px;
        }
    </style>
    <%List<Goods> list=(List<Goods>)session.getAttribute("list");%>
</head>
<body>
<div>
    <h3>购物车</h3>
    <%if(list!=null){%>
    <p>商品数量:<%=list.size()%></p>
    <table>
        <thead>
        <tr>
            <th>商品名称</th>
            <th>数量</th>
            <th>价格</th>
        </tr>
        <%for(int i=0;i<list.size();i++){%>
        <tr>
        <td><%=list.get(i).getGoodsName()%></td>
        <td><%=list.get(i).getGoodsNum()%></td>
        <td><%=list.get(i).getGoodsprice()%></td>
        </tr>
        <%}%>
        </thead>
    </table>
   <% }%>

</div>
<div id="div">
    <div id="div1">
        <p>商品1</p>
        数量:<span>1</span><br/>
        <a href="/car?goodsName=商品1&goodsNum=1">添加到购物车</a>
    </div>
    <div id="div2">
        <p>商品2</p>
        数量:<span>1</span><br/>
        <a href="/car?goodsName=商品2&goodsNum=1">添加到购物车</a>
    </div>
    <div id="div3">
        <p>商品3</p>
        数量:<span>1</span><br/>
        <a href="/car?goodsName=商品3&goodsNum=1">添加到购物车</a>
    </div>
    <div id="div4">
        <p>商品4</p>
        数量:<span>1</span><br/>
        <a href="/car?goodsName=商品4&goodsNum=1">添加到购物车</a>
    </div>
</div>

</body>
</html>
