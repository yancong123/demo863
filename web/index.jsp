<%@ page import="java.util.Date" %>
<%@ page import="java.text.SimpleDateFormat" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: yc
  Date: 2021/1/13
  Time: 11:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>$Title$</title>
      <%--cookie:保存在浏览器,不安全,不建议保存敏感信息，生命周期：从创建开始，到指定时间后销毁,cookie中保存的是键值映射--%>
<%
    String account="";
    String pwd="";
    /*获取当前域所有的cookie信息*/
   Cookie [] cookies=request.getCookies();
   /*判断cookies不为空*/
   if(cookies!=null){
       for(int i=0;i<cookies.length;i++){
           if("account".equals(cookies[i].getName())){
               account=cookies[i].getValue();
           }else if("pwd".equals(cookies[i].getName())){
               pwd=cookies[i].getValue();
           };

       }
   }
   /*
   * request.getServerName() 获取服务器地址(名称)
   * +request.getServerPort() 获取项目的端口号
   * request.getContextPath() 获取项目名称
   * */
    String url="http://"+request.getServerName()+":"+request.getServerPort()+request.getContextPath();
    %>
      <%--相对路径：相对于当前文件的目标资源路径（少）
       绝对路径：相对于整个项目的路径
       http://localhost:8080/demo--%>
    <script type="text/javascript" src="<%=url%>/js/index.js"></script>
  </head>
  <body>

  <a href="/student">查询学生信息</a>
  <%--这是一个注释--%>
  <!--这是另一个注释-->
 <h3>欢迎登陆系统</h3>
  <a href="/myServlet?name=张三&age=20">点击一下</a>
 <form action="/login" method="post">
     账号： <input type="text" name="account" value="<%=account%>" /><br>
     密码： <input type="password" name="pwd" value="<%=pwd%>"/><br>
    <%-- <input type="checkbox" name="remberPwd"/>记住密码<br>--%>
     <select name="remberPwd">
         <option value="0">不保存</option>
         <option value="7">保存7天</option>
         <option value="14">保存14天</option>
         <option value="30">保存30天</option>
     </select>
     <input type="submit">
 </form>
  <%--get和post区别
  get将请求参数拼接在url地址之后，有长度限制，不安全，效率相对较高
  post将请求参数添加到请求头的form Data中，没有长度限制，相对安全，对提交内容重新编码
  --%>


<a href="/emp">查询员工信息</a>



<%--声明一个方法--%>
  <%!
      String formatDate(Date d){
      SimpleDateFormat formater = new SimpleDateFormat("yyyy年MM月dd日");
          String format = formater.format(d);
          return format;
      }
  %>
  你好，今天是
  <%=formatDate(new Date()) %>
  <%--跳转到指定页面--%>
  <%--<jsp:forward page="index1.jsp"></jsp:forward>--%>
  <div>
      <jsp:include page="index1.jsp"></jsp:include>
  </div>
  <%out.print("我是out的输出内容");%>
  <%--<%response.sendRedirect("success.jsp");%>--%>
  <%List<String> list=(List<String>)application.getAttribute("list");
  /*转发*/
     /* RequestDispatcher requestDispatcher = application.getRequestDispatcher("/success.jsp");
      requestDispatcher.forward(request,response);*/
     /*获取服务器信息*/
      out.print(application.getServerInfo());
      out.print("<br/>"+request.getServerPort());
      out.print("<br/>"+request.getServerName());
      out.print("<br/>"+request.getLocalAddr());
      out.print("<br/>"+request.getContextPath());

  %>
  <%=url%>
  </body>
</html>
