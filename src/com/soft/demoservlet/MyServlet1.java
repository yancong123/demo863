package com.soft.demoservlet;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet
 * @ClassName: MyServlet1
 * @Author: yc
 * @Description: Servlet演示
 * @Date: 2021/1/14 10:57
 * @Version: v1.0
 * 转发和重定向的区别
 * 重定向：是response对象中的方法，浏览器重新发起请求，携带参数拼接在地址之后(不能携带参数),跳转资源通过getParameter()获取，整个业务，
 * 浏览器发起两次请求，地址栏发生改变，跨域访问
 * 转发：是request对象中的方法，服务器转发，通过在request对象中添加属性实现(调用setAttribute())携带参数，跳转资源通过getAttribute()获取(可以携带参数)，
 * 整个业务，浏览器发起一次请求，转发地址不变，不可以跨域访问
 */
public class MyServlet1 extends HttpServlet {
   /* @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }*/

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        /*设置请求参数的编码格式，必须出现在req对象调用之前*/
        req.setCharacterEncoding("UTF-8");
        /*ServletContext项目的上下文信息，所有Servlet对象共享*/
        ServletContext servletContext = req.getServletContext();
        String contextName =(String)servletContext.getAttribute("contextName");
        System.out.println(contextName);
        /*通过请求获取前台传递的参数*/
        String age = req.getParameter("age");
        String name = req.getParameter("name");
        /*从数据库查到的真实姓名*/
        String name1="张三1";
        /*向req对象的属性中添加内容*/
        req.setAttribute("name1",name1);
        if("张三".equals(name)){
            /*转发：将请求转发给另一个资源*/
            req.getRequestDispatcher("success.jsp").forward(req,resp);
        }else{
            req.getRequestDispatcher("fail.jsp").forward(req,resp);
        }

       /* if("张三".equals(name)){
            *//*重定向:告诉浏览器重新请求服务器上的其他资源*//*
            resp.sendRedirect("success.jsp?name="+name);
        }else{
            resp.sendRedirect("fail.jsp?name="+name);
        }*/
       /* *//*设置相应内容的编码格式*//*
        resp.setCharacterEncoding("UTF-8");
        *//*设置相应页面的编码格式*//*
        resp.setContentType("text/html;charset=UTF-8");
        resp.getWriter().write("name="+name+",age="+age);*/
    }
}
