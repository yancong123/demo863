package com.soft.demoservlet.controller;

import com.alibaba.fastjson.JSONObject;
import com.soft.demoservlet.entity.Area;
import com.soft.demoservlet.service.AddressService;
import com.soft.demoservlet.service.impl.AddressServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: AddressServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 16:59
 * @Version: v1.0
 */
@WebServlet("/address")
public class AddressServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String area = req.getParameter("area");
        String parentNum = req.getParameter("parentNum");
        AddressService service=new AddressServiceImpl();
        List<Area> area1 = service.findArea(area,parentNum);
        resp.getWriter().write(JSONObject.toJSONString(area1));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
