package com.soft.demoservlet.controller;

import com.soft.demoservlet.entity.Goods;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: CarServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/19 16:23
 * @Version: v1.0
 */
@WebServlet("/car")
public class CarServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String goodsName = req.getParameter("goodsName");
        String goodsNum = req.getParameter("goodsNum");
        String goodsPrice = req.getParameter("goodsPrice");
        Goods goods = new Goods();
        goods.setGoodsName(goodsName);
        goods.setGoodsNum(goodsNum);
        goods.setGoodsprice(goodsPrice);
        HttpSession session = req.getSession();
        /*提升作用域*/
        List<Object> list=null;
        if( session.getAttribute("list")==null){
            list= new ArrayList<>();
            list.add(goods);

        }else{
             list = (List<Object>)session.getAttribute("list");

            list.add(goods);

        }
        session.setAttribute("list",list);
        System.out.println(goodsName+goodsNum);
        resp.sendRedirect("goods.jsp");
    }
}
