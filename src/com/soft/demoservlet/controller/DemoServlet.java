package com.soft.demoservlet.controller;

import com.soft.demoservlet.entity.Emp;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: DemoServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/20 15:30
 * @Version: v1.0
 */
@WebServlet("/demo")
public class DemoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       /* req.setAttribute("name","Tom");*/
        Emp emp = new Emp();
        emp.setEno(1001);
        emp.setEname("张三");
        emp.setEsex("女");
        emp.setEage(20);
        req.setAttribute("emp",emp);
        req.setAttribute("score",95);
        List<Object> list = new ArrayList<>();
        list.add(emp);
        list.add(emp);list.add(emp);
        list.add(emp);list.add(emp);

        req.setAttribute("list",list);
        req.getSession().setAttribute("message","这是一个消息");
        req.getSession().setAttribute("name","JARY");
        req.getRequestDispatcher("index2.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
