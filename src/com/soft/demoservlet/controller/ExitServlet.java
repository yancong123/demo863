package com.soft.demoservlet.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: ExitServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/19 17:15
 * @Version: v1.0
 */
@WebServlet("/exit")
public class ExitServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ServletContext servletContext = req.getServletContext();
        HttpSession session = req.getSession();
        /*获取当前登录用户账号信息*/
        String account =(String) session.getAttribute("account");
        List<String> list =(List<String>) servletContext.getAttribute("list");
        for (String ac : list) {
            if(ac.equals(account)){
                list.remove(ac);
                /*当账号拼配时结束循环*/
                break;
            }
        }
        servletContext.setAttribute("list",list);
        resp.getWriter().write("退出成功");
    }
}
