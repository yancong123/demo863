package com.soft.demoservlet.controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: LoginServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/18 17:18
 * @Version: v1.0
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String account = req.getParameter("account");
        String pwd = req.getParameter("pwd");
        /*是否记住密码*/
        String rp = req.getParameter("remberPwd");
        if("admin".equals(account)&&"123456".equals(pwd)){
            req.getCookies();
            /*创建一个cookie保存账号信息*/
            Cookie cookie=new Cookie("account",account);
            /*创建一个cookie保存密码信息*/
            Cookie cookie1=new Cookie("pwd",pwd);
            Integer res = Integer.valueOf(rp);
            if(res==7){
                cookie.setMaxAge(60*60*24*7);
                cookie1.setMaxAge(60*60*24*7);
            }else if(res==14){
                cookie.setMaxAge(60*60*24*14);
                cookie1.setMaxAge(60*60*24*14);
            }else if(res==30){
                cookie.setMaxAge(60*60*24*30);
                cookie1.setMaxAge(60*60*24*30);
            }
            /*将cookie信息相应给浏览器*/
            resp.addCookie(cookie);
            resp.addCookie(cookie1);
        }

        System.out.println("账号："+account+"密码："+pwd);
        req.getRequestDispatcher("success.jsp").forward(req,resp);
    }
}
