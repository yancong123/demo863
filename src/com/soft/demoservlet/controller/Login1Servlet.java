package com.soft.demoservlet.controller;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: LoginServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/18 17:18
 * @Version: v1.0
 * session作用域:默认会话从第一次访问页面开始，到关闭浏览器结束，session对象在整个会话中有效
 * session生命周期:从第一次访问页面时创建，默认浏览器关闭时销毁
 * session:保存在服务器中，可以保存对象信息，相对安全
 * 设置session的存活时间setMaxInactiveInterval(int second)：从最后一次操作开始计算
 */
@WebServlet("/login1")
public class Login1Servlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String account = req.getParameter("account");
        String pwd = req.getParameter("pwd");
        /*获取session对象,如果存在就获取，否则创建并返回*/
        HttpSession session = req.getSession(true);
        /*向session对象中添加属性*/
        session.setAttribute("account",account);
        /*设置session的存活时间：从最后一次操作开始计算*/
        /*session.setMaxInactiveInterval(10);*/
        System.out.println(session.getId());
        System.out.println(session.getCreationTime());
        Date date = new Date(session.getCreationTime());
        System.out.println(date);
        System.out.println(session.getLastAccessedTime());
        System.out.println(session.getMaxInactiveInterval());
        /*销毁session*/
        session.invalidate();
        req.getRequestDispatcher("success.jsp").forward(req,resp);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String account = req.getParameter("account");
        String pwd = req.getParameter("pwd");
        ServletContext servletContext = req.getServletContext();
        List<String> list=null;
        int num=1;
        if(servletContext.getAttribute("list")==null){

            list=new ArrayList<>();
            list.add(account);
           /* servletContext.setAttribute("num",num);*/
        }else{
            list =( List<String>) servletContext.getAttribute("list");
            for (String account1 : list) {
                if(account1.equals(account)){
                    req.getRequestDispatcher("fail.jsp").forward(req,resp);
                }
            }
           /* num = (Integer) servletContext.getAttribute("num");*/

            num++;
            servletContext.setAttribute("num",num);
            list.add(account);

    }
        HttpSession session = req.getSession();
        session.setAttribute("account",account);
        servletContext.setAttribute("list",list);
        /*System.out.println("当前在线用户数:"+num);*/
        req.getRequestDispatcher("success.jsp").forward(req,resp);
    }
}
