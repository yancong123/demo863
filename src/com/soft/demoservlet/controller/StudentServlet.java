package com.soft.demoservlet.controller;

import com.alibaba.fastjson.JSONObject;
import com.soft.demoservlet.entity.Student;
import com.soft.demoservlet.service.StudentService;
import com.soft.demoservlet.service.impl.StudentServiceImpl;
import javafx.collections.ObservableMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: StudentServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 13:55
 * @Version: v1.0
 */
@WebServlet("/student")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*设置编码格式*/
        req.setCharacterEncoding("UTF-8");
        String method = req.getParameter("method");
        StudentService studentService=new StudentServiceImpl();
        if(method==null){
            req.getRequestDispatcher("WEB-INF/student/student.jsp").forward(req,resp);
        }else if("add".equals(method)){
            /*当method==add时，跳转到添加页面*/
        }else if("addData".equals(method)){
            /*当method==addData时，将学生数据添加到数据库*/
        }else if("delete".equals(method)){
            /*当method==delete时，执行删除操作*/
        }else if("page".equals(method)){
            /*当method==null执行查询全部的操作*/
            String page = req.getParameter("page");
            String pageSize = req.getParameter("pageSize");
            findAllPage(studentService,req,resp,page,pageSize);
        }
    }
    public void findAllPage(StudentService studentService,HttpServletRequest req, HttpServletResponse resp, String page,String pageSize) throws ServletException, IOException{
        Map map=new HashMap<String, Object>();
        List<Student> students = studentService.findAll(page,pageSize);
        map.put("students",students);
        int count=studentService.count();
        map.put("count",count);
        /*js对象和json对象的区别
        json对象:属性和字符串类型的值都使用双引号表示
        js对象：属性可以不适用双引号表示*/
       resp.getWriter().write(JSONObject.toJSONString(map));
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }
}
