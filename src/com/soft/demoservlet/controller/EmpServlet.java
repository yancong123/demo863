package com.soft.demoservlet.controller;

import com.soft.demoservlet.entity.Emp;
import com.soft.demoservlet.service.EmpService;
import com.soft.demoservlet.service.impl.EmpServiceImpl;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: EmpServlet
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:31
 * @Version: v1.0
 * 当请求没有携带method参数执行查询全部的操作
 */
@WebServlet("/emp")
public class EmpServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        /*resp.sendRedirect("UTF-8");*/
        String method = req.getParameter("method");
        /*判断用户请求*/
        int res=judgeMethod(method,req,resp);
        result(req,res);
        req.getRequestDispatcher("result.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        doGet(req,resp);
    }

    /**
     * 判断用户请求
     * @param method
     * @param req
     * @param resp
     * @return
     * @throws ServletException
     * @throws IOException
     */
    public int judgeMethod(String method,HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        EmpService service=new EmpServiceImpl();
        if(method==null||"".equals(method)){
            findAll(req,resp,service);
        }else if("delete".equals(method)){
            return delete(req,resp,service);
        }else if("update".equals(method)){
            findOne(req,resp,service);
        }else if("add".equals(method)){
            req.getRequestDispatcher("WEB-INF/add.jsp").forward(req,resp);
        }else if("addData".equals(method)){
            return addEmp(req,resp,service);
        }else if("updateData".equals(method)){
            return updateEmpByEno(req,resp,service);
        }
        return 0;
    }
    /**
     * 处理增删改的相应页面
     * @param req
     * @param res
     */
    public void result(HttpServletRequest req, int res){
        String message="操作失败";
        if(res>0){
            message="操作成功";
        }
        req.setAttribute("message",message);
        req.setAttribute("requestUrl","/emp");
    }

    /**
     * 查询所有员工信息
     * @param req
     * @param resp
     * @param service
     * @throws ServletException
     * @throws IOException
     */
    public void findAll(HttpServletRequest req, HttpServletResponse resp,EmpService service) throws ServletException, IOException {
        List<Emp> emps = service.findAll();
        /*添加属性*/
        req.setAttribute("emps",emps);
       /* *//*替换属性*//*
        req.setAttribute("emps",emps);
        *//*移除属性*//*
        req.removeAttribute("emps");*/
        req.getRequestDispatcher("WEB-INF/emp.jsp").forward(req,resp);
    }

    /**
     * 删除员工信息
     * @param req
     * @param resp
     * @param service
     * @return
     */
    public int delete(HttpServletRequest req, HttpServletResponse resp,EmpService service){
        String eno = req.getParameter("eno");
        return service.deleteByEno(eno);
    }

    /**
     * 根据工号查询单个员工信息
     * @param req
     * @param resp
     * @param service
     * @throws ServletException
     * @throws IOException
     */
    public void findOne(HttpServletRequest req, HttpServletResponse resp,EmpService service) throws ServletException, IOException {
        String eno = req.getParameter("eno");
        Emp emp=service.findOne(eno);
        req.setAttribute("emp",emp);
        req.getRequestDispatcher("WEB-INF/update.jsp").forward(req,resp);
    }

    /**
     * 添加员工信息
     * @param req
     * @param resp
     * @param service
     * @return
     */
    public int addEmp(HttpServletRequest req, HttpServletResponse resp,EmpService service){
        Emp emp = new Emp();
        String eno = req.getParameter("eno");
        if(! "".equals(eno)){
            if(eno.matches("[0-9]{4}")){
                emp.setEno(Integer.valueOf(eno));
            }
        }

        emp.setEname(req.getParameter("ename"));
        emp.setEage(Integer.valueOf(req.getParameter("eage")));
        emp.setEsex(req.getParameter("esex"));
        return service.add(emp);
    }

    /**
     * 根据工号修改员工信息
     * @param req
     * @param resp
     * @param service
     * @return
     */
    public int updateEmpByEno(HttpServletRequest req, HttpServletResponse resp,EmpService service){
        Emp emp = new Emp();
        String eno = req.getParameter("eno");
        emp.setEno(Integer.valueOf(eno));
        emp.setEname(req.getParameter("ename"));
        emp.setEage(Integer.valueOf(req.getParameter("eage")));
        emp.setEsex(req.getParameter("esex"));
        return service.updateByEno(emp);
    }
}
