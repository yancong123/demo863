package com.soft.demoservlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet
 * @ClassName: MyServlet
 * @Author: yc
 * @Description: Servlet测试
 * @Date: 2021/1/13 11:36
 * @Version: v1.0
 *单例模式：整个项目执行期间只有一个对象
 * 懒汉式：节省内存空间，效率低
 * 饿汉式：占用内存空间，效率高
 * Servlet生命周期
 * 在初次访问Servlet创建Servlet对象，调用service()判断用户请求，并匹配执行doGet()或doPost()方法，当正常关闭服务器时销毁Servlet对象
 * Servlet执行流程
 * 用户发起请求，tomcat服务器解析地址找到对应的Servlet,执行service(),判断用户请求，并匹配执行doGet()或doPost()方法，方法执行结束后，tomcat返回结果给用户
 */

public class MyServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        System.out.println("name:"+name+"age:"+age);
        /*get：在地址栏直接输入地址
        * 超链接
        * form的get请求*/
        System.out.println("处理前台的get请求");
        /*获取响应流*/
        PrintWriter writer = resp.getWriter();
        writer.write("我是后台响应的内容");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("处理前台的post请求");
    }

    @Override
    protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("分析用户请求，调用对应方法");
        super.service(req, resp);
    }

    @Override
    public void destroy() {
        System.out.println("Servlet销毁时调用");
    }


    @Override
    public void init(ServletConfig config) throws ServletException {
        String value = config.getInitParameter("value");
        System.out.println(value);
        ServletContext servletContext = config.getServletContext();
        servletContext.setAttribute("contextName","context1");
        String encoding = servletContext.getInitParameter("encoding");
        System.out.println(encoding);
        System.out.println("Servlet初始化时调用");
    }
}
