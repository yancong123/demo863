package com.soft.demoservlet.entity;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.entity
 * @ClassName: Area
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 17:13
 * @Version: v1.0
 */
public class Area {
    private int id;
    private int areaNum;
    private String type;
    private String areaName;
    private int parentNum;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAreaNum() {
        return areaNum;
    }

    public void setAreaNum(int areaNum) {
        this.areaNum = areaNum;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public int getParentNum() {
        return parentNum;
    }

    public void setParentNum(int parentNum) {
        this.parentNum = parentNum;
    }
}
