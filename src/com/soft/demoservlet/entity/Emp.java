package com.soft.demoservlet.entity;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.entity
 * @ClassName: Emp
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:51
 * @Version: v1.0
 */
public class Emp {
    private int eno;
    private String ename;
    private int eage;
    private String esex;

    public int getEno() {
        return eno;
    }

    public void setEno(int eno) {
        this.eno = eno;
    }

    public String getEname() {
        return ename;
    }

    public void setEname(String ename) {
        this.ename = ename;
    }

    public int getEage() {
        return eage;
    }

    public void setEage(int eage) {
        this.eage = eage;
    }

    public String getEsex() {
        return esex;
    }

    public void setEsex(String esex) {
        this.esex = esex;
    }
}
