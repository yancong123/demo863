package com.soft.demoservlet.entity;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.entity
 * @ClassName: Goods
 * @Author: yc
 * @Description:
 * @Date: 2021/1/19 16:28
 * @Version: v1.0
 */
public class Goods {
    private String goodsName;
    private String goodsNum;
    private String goodsprice;

    public String getGoodsprice() {
        return goodsprice;
    }

    public void setGoodsprice(String goodsprice) {
        this.goodsprice = goodsprice;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsNum() {
        return goodsNum;
    }

    public void setGoodsNum(String goodsNum) {
        this.goodsNum = goodsNum;
    }
}
