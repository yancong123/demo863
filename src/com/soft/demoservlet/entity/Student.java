package com.soft.demoservlet.entity;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.entity
 * @ClassName: Student
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 14:17
 * @Version: v1.0
 */
public class Student {
   private int id;
    private int sno;
    private String sname;
    private int sage;
    private int ssex;
    private String insertime;
    private String insertuser;
    private String updatetime;
    private String updateuser;
    private int deleteStatus;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getSno() {
        return sno;
    }

    public void setSno(int sno) {
        this.sno = sno;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public int getSage() {
        return sage;
    }

    public void setSage(int sage) {
        this.sage = sage;
    }

    public int getSsex() {
        return ssex;
    }

    public void setSsex(int ssex) {
        this.ssex = ssex;
    }

    public String getInsertime() {
        return insertime;
    }

    public void setInsertime(String insertime) {
        this.insertime = insertime;
    }

    public String getInsertuser() {
        return insertuser;
    }

    public void setInsertuser(String insertuser) {
        this.insertuser = insertuser;
    }

    public String getUpdatetime() {
        return updatetime;
    }

    public void setUpdatetime(String updatetime) {
        this.updatetime = updatetime;
    }

    public String getUpdateuser() {
        return updateuser;
    }

    public void setUpdateuser(String updateuser) {
        this.updateuser = updateuser;
    }

    public int getDeleteStatus() {
        return deleteStatus;
    }

    public void setDeleteStatus(int deleteStatus) {
        this.deleteStatus = deleteStatus;
    }
}
