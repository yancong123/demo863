package com.soft.demoservlet.filter;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.controller
 * @ClassName: MyFilter
 * @Author: yc
 * @Description: 过滤器
 * @Date: 2021/1/18 11:42
 * @Version: v1.0
 */
public class MyFilter extends HttpFilter {
    String encoding;
    int a=0;
    @Override
    protected void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {

        /*设置编码格式*/
       request.setCharacterEncoding(encoding);
        response.setCharacterEncoding(encoding);
        response.setContentType("text/html;charset="+encoding);
        /*如果存在其他过滤器则执行其他过滤器，否则访问Servlet资源*/
        /*a为偶数执行业务处理，否则返回首页*/
       /* if(a%2==0){*/
            chain.doFilter(request, response);
          /*  String method = request.getParameter("method");
            System.out.println(method);
            response.getWriter().write("过滤器拦截了相应");*/
       /* }else{
            response.sendRedirect("index.jsp");
        }*/
       /* a++;
        System.out.println("访问资源结束");*/
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("encoding");
    }
}
