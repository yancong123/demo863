package com.soft.demoservlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet
 * @ClassName: MyServlet2
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 15:43
 * @Version: v1.0
 */
public class MyServlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        StringBuffer requestURL = req.getRequestURL();//获取请求的url地址
        String requestURI = req.getRequestURI();//获取请求的资源地址(不包含域名)
        String queryString = req.getQueryString();//方法返回请求行中的参数部分
        String remoteAddr = req.getRemoteAddr();
        String remoteHost = req.getRemoteHost();
        int remotePort = req.getRemotePort();
        String localAddr = req.getLocalAddr();
        String localName = req.getLocalName();
        String method = req.getMethod();
        String[] names = req.getParameterValues("name");//获取相同属性的所有值
        /*获取前台提交的参数*/
        Map<String, String[]> parameterMap = req.getParameterMap();
        Set<String> strings = parameterMap.keySet();
        Iterator<String> iterator = strings.iterator();
        while (iterator.hasNext()){
            String next = iterator.next();
            String[] strings1 = parameterMap.get(next);
            for (int i = 0; i < strings1.length; i++) {
                System.out.println(strings1[i]);
            }
        }


    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
       doGet(req,resp);
    }
}
