package com.soft.demoservlet.dao;

import com.soft.demoservlet.entity.Area;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao
 * @ClassName: AddressDao
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 17:04
 * @Version: v1.0
 */
public interface AddressDao {
    /**
     * 根据标签查询地区
     * @param areaPag
     * @return
     */
    List<Area> findArea(String areaPag,String parentNum) throws SQLException;
}
