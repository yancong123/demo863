package com.soft.demoservlet.dao;

import com.soft.demoservlet.entity.Emp;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao
 * @ClassName: EmpDao
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:37
 * @Version: v1.0
 */
public interface EmpDao {
    /**
     * 查询所有员工信息
     * @return
     */
    List<Emp> findAll() throws SQLException;

    /**
     * 根据工号删除员工信息
     * @param eno 工号
     * @return 删除条数
     */
    int deleteByEno(String eno) throws  Exception;

    /**
     * 添加员工信息
     * @param emp 员工信息
     * @return 添加条数
     */
    int add(Emp emp) throws Exception;
    /**
     * 根据工号查询员工信息
     * @param eno 工号
     * @return 员工信息
     */
    Emp findOne(String eno)throws Exception;

    /**
     * 根据工号修改员工信息
     * @param emp 员工信息
     * @return 修改条数
     * @throws Exception
     */
    int updateByEno(Emp emp)throws Exception;
}
