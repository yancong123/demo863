package com.soft.demoservlet.dao.impl;

import com.soft.demoservlet.dao.EmpDao;
import com.soft.demoservlet.entity.Emp;
import com.soft.demoservlet.util.DbUtil;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao.impl
 * @ClassName: EmpDaoImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:39
 * @Version: v1.0
 */
public class EmpDaoImpl implements EmpDao {

    @Override
    public List<Emp> findAll() throws SQLException {
        Connection connection = DbUtil.getConnection();
        String sql="select * from emp";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        /*执行sql*/
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Emp> list=new ArrayList<>();
        /*处理结果*/
        while(resultSet.next()){
            Emp emp = new Emp();
            emp.setEno(resultSet.getInt("eno"));
            emp.setEname(resultSet.getString("ename"));
            emp.setEage(resultSet.getInt("eage"));
            emp.setEsex(resultSet.getString("esex"));
            list.add(emp);
        }
        DbUtil.close(connection,preparedStatement,resultSet);
        return list;
    }

    @Override
    public int deleteByEno(String eno) throws Exception {
        Connection connection = DbUtil.getConnection();
        String sql="delete from emp where eno=?";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,eno);

        /*执行sql*/
        int res = preparedStatement.executeUpdate();
        DbUtil.close(connection,preparedStatement);
        return res;
    }

    @Override
    public int add(Emp emp) throws Exception{
        Connection connection = DbUtil.getConnection();
        String sql="insert into emp(eno,ename,eage,esex) values(?,?,?,?)";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,emp.getEno());
        preparedStatement.setInt(3,emp.getEage());
        preparedStatement.setString(2,emp.getEname());
        preparedStatement.setString(4,emp.getEsex());
        /*执行sql*/
        int res = preparedStatement.executeUpdate();
        DbUtil.close(connection,preparedStatement);
        return res;
    }

    @Override
    public Emp findOne(String eno) throws Exception {
        Connection connection = DbUtil.getConnection();
        String sql="select * from emp where eno=?";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,eno);
        /*执行sql*/
        ResultSet resultSet = preparedStatement.executeQuery();
        Emp emp=null;
        /*处理结果*/
        while(resultSet.next()){
            emp = new Emp();
            emp.setEno(resultSet.getInt("eno"));
            emp.setEname(resultSet.getString("ename"));
            emp.setEage(resultSet.getInt("eage"));
            emp.setEsex(resultSet.getString("esex"));
        }
        DbUtil.close(connection,preparedStatement,resultSet);
        return emp;
    }

    @Override
    public int updateByEno(Emp emp) throws Exception {
        Connection connection = DbUtil.getConnection();
        String sql="update emp set ename=?,eage=?,esex=? where eno=?";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,emp.getEname());
        preparedStatement.setInt(2,emp.getEage());
        preparedStatement.setString(3,emp.getEsex());
        preparedStatement.setInt(4,emp.getEno());
        /*执行sql*/
        int res = preparedStatement.executeUpdate();
        DbUtil.close(connection,preparedStatement);
        return res;
    }
}
