package com.soft.demoservlet.dao.impl;

import com.soft.demoservlet.dao.AddressDao;
import com.soft.demoservlet.entity.Area;
import com.soft.demoservlet.entity.Emp;
import com.soft.demoservlet.util.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao.impl
 * @ClassName: AddressDaoImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 17:05
 * @Version: v1.0
 */
public class AddressDaoImpl implements AddressDao {
    @Override
    public List<Area> findArea(String areaPag,String parentNum) throws SQLException {
        Connection connection = DbUtil.getConnection();
        if(parentNum==null){
            parentNum="0";
        }
        String sql="select * from area where type=? and parentNum=?";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setString(1,areaPag);
        preparedStatement.setString(2,parentNum);
        /*执行sql*/
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Area> list=new ArrayList<>();
        /*处理结果*/
        while(resultSet.next()){
           Area area=new Area();
            area.setId(resultSet.getInt("id"));
            area.setAreaName(resultSet.getString("areaName"));
            area.setAreaNum(resultSet.getInt("areaNum"));
            area.setParentNum(resultSet.getInt("parentNum"));
            area.setType(resultSet.getString("type"));
            list.add(area);
        }
        DbUtil.close(connection,preparedStatement,resultSet);
        return list;

    }
}
