package com.soft.demoservlet.dao.impl;

import com.soft.demoservlet.dao.StudentDao;
import com.soft.demoservlet.entity.Emp;
import com.soft.demoservlet.entity.Student;
import com.soft.demoservlet.util.DbUtil;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao.impl
 * @ClassName: StudentDaoImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 14:03
 * @Version: v1.0
 */
public class StudentDaoImpl implements StudentDao {
    @Override
    public List<Student> findAll( String page,String pageSize) throws SQLException {
        Integer pageInt = Integer.valueOf(page);
        Integer pageSizeInt = Integer.valueOf(pageSize);
        int startRow=pageSizeInt*(pageInt-1);
        Connection connection = DbUtil.getConnection();
        String sql="select * from student where deleteStatus=0 limit ?,?";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        preparedStatement.setInt(1,startRow);
        preparedStatement.setInt(2,pageSizeInt);
        /*执行sql*/
        ResultSet resultSet = preparedStatement.executeQuery();
        List<Student> list=new ArrayList<>();
        /*处理结果*/
        while(resultSet.next()){
           Student student=new Student();
           student.setId(resultSet.getInt("id"));
           student.setSno(resultSet.getInt("sno"));
           student.setSname(resultSet.getString("sname"));
           student.setSage(resultSet.getInt("sage"));
           student.setSsex(resultSet.getInt("ssex"));
           student.setInsertime(resultSet.getString("insertime"));
           student.setInsertuser(resultSet.getString("insertuser"));
           student.setUpdatetime(resultSet.getString("updatetime"));
           student.setUpdateuser(resultSet.getString("updateuser"));
           student.setDeleteStatus(resultSet.getInt("deleteStatus"));
            list.add(student);
        }
        DbUtil.close(connection,preparedStatement,resultSet);
        return list;
    }

    @Override
    public int count() throws SQLException{
        Connection connection = DbUtil.getConnection();
        String sql="select count(*) count from student where deleteStatus=0";
        /*创建对象*/
        PreparedStatement preparedStatement = connection.prepareStatement(sql);
        /*执行sql*/
        ResultSet resultSet = preparedStatement.executeQuery();
        int count = resultSet.next() ? resultSet.getInt("count") : 0;
        DbUtil.close(connection,preparedStatement,resultSet);
        return count;
    }
}
