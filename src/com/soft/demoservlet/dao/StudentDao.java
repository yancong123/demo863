package com.soft.demoservlet.dao;

import com.soft.demoservlet.entity.Student;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.dao
 * @ClassName: StudentDao
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 14:03
 * @Version: v1.0
 */
public interface StudentDao {
    /**
     * 查询所有学生信息
     * @return 所有学生信息
     */
    public List<Student> findAll( String page,String pageSize) throws SQLException;

    int count() throws SQLException;
}
