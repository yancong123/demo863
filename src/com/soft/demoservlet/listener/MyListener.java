package com.soft.demoservlet.listener;

import javax.servlet.ServletRequestAttributeEvent;
import javax.servlet.ServletRequestAttributeListener;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.listener
 * @ClassName: MyListener
 * @Author: yc
 * @Description:
 * @Date: 2021/1/18 16:34
 * @Version: v1.0
 */
public class MyListener implements ServletRequestListener, ServletRequestAttributeListener {
    @Override
    public void requestDestroyed(ServletRequestEvent sre) {
        System.out.println("监听Request对象销毁操作");
    }

    @Override
    public void requestInitialized(ServletRequestEvent sre) {
        System.out.println("监听Request的创建操作");
    }

    @Override
    public void attributeAdded(ServletRequestAttributeEvent srae) {
        System.out.println("监听request对象添加属性后");
    }

    @Override
    public void attributeRemoved(ServletRequestAttributeEvent srae) {
        System.out.println("监听request对象移除属性后");
    }

    @Override
    public void attributeReplaced(ServletRequestAttributeEvent srae) {
        System.out.println("监听request对象替换属性后");
    }
}
