package com.soft.demoservlet.listener;

import javax.servlet.ServletContextAttributeEvent;
import javax.servlet.ServletContextAttributeListener;
import javax.servlet.annotation.WebListener;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.listener
 * @ClassName: MyApplicationListener
 * @Author: yc
 * @Description:
 * @Date: 2021/1/19 17:10
 * @Version: v1.0
 */
/*@WebListener*/
public class MyApplicationListener implements ServletContextAttributeListener {
    @Override
    public void attributeAdded(ServletContextAttributeEvent scae) {
        List<String> list =(List<String>) scae.getServletContext().getAttribute("list");
        System.out.println("当前登录用户数："+list.size());
    }


    @Override
    public void attributeReplaced(ServletContextAttributeEvent scae) {
        List<String> list =(List<String>) scae.getServletContext().getAttribute("list");
        System.out.println("当前登录用户数："+list.size());
    }
}
