package com.soft.demoservlet.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.util
 * @ClassName: DbUtil
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:55
 * @Version: v1.0
 */
public class DbUtil {
    /**
     * 建立连接
     * @return
     */
    public static Connection getConnection(){
        Connection connection=null;
        try{
            /*加载驱动*/
            Class.forName("com.mysql.cj.jdbc.Driver");
            String url="jdbc:mysql://localhost:3306/demo288?serverTimezone=UTC&charSet=UTF-8&useSSL=false";
            String user="root";
            String pwd="123456";
            /*建立连接*/
           connection = DriverManager.getConnection(url, user, pwd);
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("建立连接失败");
        }
        return connection;
    }

    public static void close(Connection con, Statement statement, ResultSet set){
        try{
            if(set!=null){
                set.close();
            }
        }catch (Exception e){
            System.out.println("结果集关闭失败");
        }finally {
            close(con,statement);
        }



    }
    public static void close(Connection con, Statement statement){

            try{
                if(statement!=null){
                    statement.close();
                }
            }catch (Exception e){
                System.out.println("Statememt对象关闭失败");
            }finally {
                try {
                    if(con!=null){
                        con.close();
                    }
                }catch (Exception e){
                    System.out.println("连接关闭失败");
                }

            }
        }
}
