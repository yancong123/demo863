package com.soft.demoservlet.service;

import com.soft.demoservlet.entity.Area;

import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service
 * @ClassName: AddressService
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 17:01
 * @Version: v1.0
 */
public interface AddressService {
    /**
     * 根据标签查询地区
     * @param areaPag
     * @return
     */
    List<Area> findArea(String areaPag,String parentNum);
}
