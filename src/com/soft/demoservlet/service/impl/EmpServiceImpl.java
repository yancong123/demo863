package com.soft.demoservlet.service.impl;

import com.soft.demoservlet.dao.EmpDao;
import com.soft.demoservlet.dao.impl.EmpDaoImpl;
import com.soft.demoservlet.entity.Emp;
import com.soft.demoservlet.service.EmpService;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service.impl
 * @ClassName: EmpServiceImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:35
 * @Version: v1.0
 */
public class EmpServiceImpl implements EmpService {
    EmpDao dao=new EmpDaoImpl();
    @Override
    public List<Emp> findAll() {
        List<Emp> emps = null;
        try {
            emps = dao.findAll();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return emps;
    }

    @Override
    public int deleteByEno(String eno) {
        int res = 0;
        try {
            res = dao.deleteByEno(eno);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public int add(Emp emp) {
        int res = 0;
        try {
            res = dao.add(emp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }

    @Override
    public Emp findOne(String eno) {
        Emp emp = null;
        try {
            emp = dao.findOne(eno);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return emp;
    }

    @Override
    public int updateByEno(Emp emp) {
        int res = 0;
        try {
            res = dao.updateByEno(emp);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return res;
    }
}
