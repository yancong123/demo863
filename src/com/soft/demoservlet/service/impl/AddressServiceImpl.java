package com.soft.demoservlet.service.impl;

import com.soft.demoservlet.dao.AddressDao;
import com.soft.demoservlet.dao.impl.AddressDaoImpl;
import com.soft.demoservlet.entity.Area;
import com.soft.demoservlet.service.AddressService;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service
 * @ClassName: AddressServiceImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 17:04
 * @Version: v1.0
 */
public class AddressServiceImpl implements AddressService {
    AddressDao addresssDao=new AddressDaoImpl();
    @Override
    public List<Area> findArea(String areaPag,String parentNum) {
        List<Area> area = null;
        try {
            area = addresssDao.findArea(areaPag,parentNum);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return area;
    }
}
