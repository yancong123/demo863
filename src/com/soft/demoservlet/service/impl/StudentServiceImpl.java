package com.soft.demoservlet.service.impl;

import com.soft.demoservlet.dao.StudentDao;
import com.soft.demoservlet.dao.impl.StudentDaoImpl;
import com.soft.demoservlet.entity.Student;
import com.soft.demoservlet.service.StudentService;

import java.sql.SQLException;
import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service.impl
 * @ClassName: StudentServiceImpl
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 14:02
 * @Version: v1.0
 */
public class StudentServiceImpl implements StudentService {
    StudentDao dao=new StudentDaoImpl();
    @Override
    public List<Student> findAll( String page,String pageSize) {
        List<Student> students = null;
        try {
            students = dao.findAll(page,pageSize);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return students;
    }

    @Override
    public int count() {
        int count = 0;
        try {
            count = dao.count();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return count;
    }
}
