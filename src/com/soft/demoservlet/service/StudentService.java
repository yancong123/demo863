package com.soft.demoservlet.service;

import com.soft.demoservlet.entity.Student;

import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service
 * @ClassName: StudentService
 * @Author: yc
 * @Description:
 * @Date: 2021/1/25 14:02
 * @Version: v1.0
 */
public interface StudentService {
    /**
     * 查询所有学生信息
     * @return 所有学生信息
     */
    public List<Student> findAll( String page,String pageSize);

    int count();
}
