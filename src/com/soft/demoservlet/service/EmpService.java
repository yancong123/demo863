package com.soft.demoservlet.service;

import com.soft.demoservlet.entity.Emp;

import java.util.List;

/**
 * @ProjectName: demoservlet
 * @Package: com.soft.demoservlet.service
 * @ClassName: EmpService
 * @Author: yc
 * @Description:
 * @Date: 2021/1/14 16:33
 * @Version: v1.0
 */
public interface EmpService {
    /**
     * 查询所有员工信息
     * @return
     */
    List<Emp> findAll();

    /**
     * 根据工号删除员工信息
     * @param eno 工号
     * @return 删除条数
     */
    int deleteByEno(String eno);

    /**
     * 添加员工信息
     * @param emp 员工信息
     * @return 添加条数
     */
    int add(Emp emp);

    /**
     * 根据工号查询员工信息
     * @param eno 工号
     * @return 员工信息
     */
    Emp findOne(String eno);

    /**
     * 根据工号修改员工信息
     * @param emp 员工信息
     * @return 修改条数
     */
    int updateByEno(Emp emp);
}
